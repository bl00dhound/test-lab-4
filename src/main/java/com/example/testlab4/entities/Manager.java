package com.example.testlab4.entities;

public class Manager extends Customer {
    public Manager(String name) {
        super(name, Role.Manager);
    }

    public void printName() {
        System.out.println("I'm a Manager: " + getName());
    }

    public void changeProjectStatus(Project project, Status newStatus) throws Exception {
        if (project.checkAccess(this)) {
            printName();
            project.changeStatus(newStatus);
        }
    }

    public void increaseBalance(Project project, double amount) {
        if (project.checkAccess(this)) {
            project.increaseBalance(amount);
        }
    }

    public void decreaseBalance(Project project, double amount) {
        if (project.checkAccess(this)) {
            project.decreaseBalance(amount);
        }
    }
}
