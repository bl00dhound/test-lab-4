package com.example.testlab4.entities;

public class Project {
    private Status status;

    private Customer client;
    private String projectName;
    private double balance;

    public Project() {
        status = Status.Initial;
        balance = 0;
    }

    public void changeStatus(Status status) throws ClassCastException {
        if (getStatus().equals(Status.Completed)) {
            throw new ClassCastException("There is no way to set up status for completed project!");
        }
        this.status = status;
    }

    public boolean checkAccess(Customer customer) {
        return customer.getRole().equals(Role.Manager);
    }

    public void increaseBalance(double amount) {
        balance += amount;
    }

    public void decreaseBalance(double amount) {
        if (balance < amount) {
            balance = 0.0;
        } else {
            balance -= amount;
        }
    }

    public double getBalance() {
        return balance;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Customer getClient() {
        return client;
    }


}
