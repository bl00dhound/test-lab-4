package com.example.testlab4.entities;

public enum Role {
    Manager, Client, Developer, Owner, Designer, SeoManager, Copywriter
}
