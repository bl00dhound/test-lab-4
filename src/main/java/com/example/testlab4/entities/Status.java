package com.example.testlab4.entities;

public enum Status {
    Initial, DesignCompleted, ContentCompleted, SeoCompleted, Developed, Released, Completed
}
