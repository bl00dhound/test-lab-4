package com.example.testlab4.entities;

import org.junit.AfterClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ManagerTests {

    @AfterClass
    public static void afterClass() {
        System.out.println("All connections were closed here.");
    }

    @Test
    void shouldCreateProjectWithManager() {
        Manager manager = new Manager("John");

        Assertions.assertEquals(manager.getName(), "John");
        Assertions.assertEquals(manager.getRole(), Role.Manager);
    }

    @Test
    void shouldChangeProjectStatus() throws Exception {
        Manager manager = new Manager("John");

        Project project = new Project();
        Assertions.assertEquals(project.getStatus(), Status.Initial);

        manager.changeProjectStatus(project, Status.ContentCompleted);

        Assertions.assertEquals(project.getStatus(), Status.ContentCompleted);
        Assertions.assertNull(project.getClient());
    }

}
