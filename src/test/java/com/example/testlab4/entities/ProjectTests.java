package com.example.testlab4.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ProjectTests {

    @Test
    void shouldChangeProjectStatus() {
        Project project = new Project();

        project.changeStatus(Status.Developed);

        Assertions.assertEquals(project.getStatus(), Status.Developed);
    }

    @Test
    void shouldThrowClassCastException() {
        Project project = new Project();

        project.changeStatus(Status.Completed);
        Assertions.assertThrowsExactly(ClassCastException.class, () -> {
            project.changeStatus(Status.Initial);
        });

    }
}
